package com.crypto.crytinfo.model


data class HomeDataModel(
    var global: String,
    var cryptocurrency: String,
    var watchlist: String,
    var exchanges: String
)