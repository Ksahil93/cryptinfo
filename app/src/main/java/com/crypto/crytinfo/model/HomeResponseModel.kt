package com.crypto.crytinfo.model


data class HomeResponseModel(
    var pos:Int,
    var title: String,
    var data: List<*>
)