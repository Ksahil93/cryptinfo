package com.crypto.crytinfo.model


data class GlobalDataModel(
    var active_cryptocurrencies: Int,
    var upcoming_icos: Int,
    var ongoing_icos: Int,
    var ended_icos: Int,
    var markets: Int,
    var total_market_cap: BtcDataModel,
    var total_volume: BtcDataModel,
    var market_cap_percentage: BtcDataModel,
    var market_cap_change_percentage_24h_usd: Double,
    var updated_at: Int
)