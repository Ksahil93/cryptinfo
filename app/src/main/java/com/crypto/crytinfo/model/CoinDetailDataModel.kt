package com.crypto.crytinfo.model


data class CoinDetailDataModel(
    var id: String,
    var symbol: String,
    var name: String,
    var image: String,
    var current_price: Double,
    var price_change_percentage_24h: Double? = null,
    var sparkline_in_7d: PriceDataModel? = null,
)