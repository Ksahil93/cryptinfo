package com.crypto.crytinfo.model

data class NewsDataModel(
    var id: String,
    var searchKeyWords: ArrayList<String>,
    var feedDate: Long,
    var source: String,
    var title: String,
    var description: String,
    var imgURL: String,
    var link: String,
    var sourceLink: String,
    var shareURL: String,
    var relatedCoins: ArrayList<String>,
    var content: Boolean
)