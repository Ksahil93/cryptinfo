package com.crypto.crytinfo.model


data class CurrencyDataModel(
    var inr: Double? = null,
    var usd: Double? = null,
)