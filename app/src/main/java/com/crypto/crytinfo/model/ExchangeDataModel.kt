package com.crypto.crytinfo.model


data class ExchangeDataModel(
    var id: String,
    var name: String,
    var year_established: Int,
    var country: String,
    var description: String,
    var url: String,
    var image: String,
    var has_trading_incentive: Boolean,
    var trust_score: Int,
    var trust_score_rank: Int,
    var trade_volume_24h_btc: Double,
    var trade_volume_24h_btc_normalized: Double
)