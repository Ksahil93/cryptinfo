package com.crypto.crytinfo.model



data class BtcDataModel(
    var btc: Double,
    var eth: Double,
    var usdt: Double,
    var bnb: Double,
    var ada: Double,
    var doge: Double,
    var xpr: Double,
    var dot: Double,
    var usdc: Double,
    var uni: Double,
)