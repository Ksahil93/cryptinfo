package com.crypto.crytinfo.model


data class TrendingCoinListModel(
    var coins: List<TrendingCoinModel>
)