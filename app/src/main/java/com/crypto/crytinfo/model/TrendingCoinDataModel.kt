package com.crypto.crytinfo.model


data class TrendingCoinDataModel(
    var id: String,
    var coin_id: Int,
    var name: String,
    var symbol: String,
    var price_btc: Double,
    var small: String,
    var market_cap_rank:String
)