package com.crypto.crytinfo.model


import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.crypto.crytinfo.db.ProductTypeConverters


@Entity(tableName = "news_data")
@TypeConverters(ProductTypeConverters::class)
data class NewsModel(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var news: List<NewsDataModel>
)