package com.covid.covaxx.utils

object Constant {

    val CRYPTO = "Cryptocurrency"
    val WATCHLIST = "Watchlist"
    val TRENDING = "Trending"
    val EXCHANGE = "Exchanges"
    val GLOBAL = "Global Market Cap"

    val MARKET = "MARKET_TAG"
    val NEWS = "NEWS_TAG"
    val MORE = "MORE_TAG"


}