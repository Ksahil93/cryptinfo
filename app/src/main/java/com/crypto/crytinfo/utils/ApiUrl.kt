package com.crypto.crytinfo.utils

object ApiUrl {

    val COIN_GECKO_BASE = "https://api.coingecko.com/api/v3/"
    val COIN_CAP_BASE = "https://api.coinstats.app/public/v1/"
    val COIN_CRYPTOCURRENCY = "coins/markets?vs_currency=usd&order=market_cap_desc&sparkline=true&"
    val COIN_EXCHANGES = "exchanges?"
    val COIN_FINANCE = "finance_platforms?"
    val COIN_TRENDING = "search/trending"
    val GLOBAL = "global"
    val PAGE_PARAMS = "per_page={count}&page={pageno}"
    val NEWS = "news?skip=0&limit=20"


}