package com.crypto.crytinfo.network

import com.crypto.crytinfo.model.*
import com.crypto.crytinfo.utils.ApiUrl
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*

class ApiService {

    private val client = HttpClient(Android) {
        install(DefaultRequest) {
            headers.append("Content-Type", "application/json")
        }
        install(JsonFeature) {
            serializer = GsonSerializer()
        }
        engine {
            connectTimeout = 100_000
            socketTimeout = 100_000
        }
    }

    suspend fun getCryptocurrency(params:String): List<CoinDataModel> {
        return client.get {
            url(ApiUrl.COIN_GECKO_BASE + ApiUrl.COIN_CRYPTOCURRENCY + params)
        }
    }

    suspend fun getExchanges(params:String): List<ExchangeDataModel> {
        return client.get {
            url(ApiUrl.COIN_GECKO_BASE + ApiUrl.COIN_EXCHANGES + params)
        }
    }

    suspend fun getTrending(): TrendingCoinListModel {
        return client.get {
            url(ApiUrl.COIN_GECKO_BASE + ApiUrl.COIN_TRENDING)
        }
    }
    suspend fun getGlobal(): GlobalModel {
        return client.get {
            url(ApiUrl.COIN_GECKO_BASE + ApiUrl.GLOBAL)
        }
    }
    suspend fun getNews(): NewsModel {
        return client.get {
            url(ApiUrl.COIN_CAP_BASE + ApiUrl.NEWS)
        }
    }
}