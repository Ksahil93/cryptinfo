package com.crypto.crytinfo.network

import com.crypto.crytinfo.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class CryptoRepository {

    private val apiService = ApiService()

    fun getCryptocurrency(params: String): Flow<List<CoinDataModel>> = flow {
        emit(apiService.getCryptocurrency(params))
    }.flowOn(Dispatchers.IO)

    fun getExchange(params: String): Flow<List<ExchangeDataModel>> = flow {
        emit(apiService.getExchanges(params))
    }.flowOn(Dispatchers.IO)

    fun getTrending(): Flow<TrendingCoinListModel> = flow {
        emit(apiService.getTrending())
    }.flowOn(Dispatchers.IO)

    fun getGlobal(): Flow<GlobalModel> = flow {
        emit(apiService.getGlobal())
    }.flowOn(Dispatchers.IO)

    fun getNews(): Flow<NewsModel> = flow {
        emit(apiService.getNews())
    }.flowOn(Dispatchers.IO)

}