package com.crypto.crytinfo.network


sealed class ApiStatus {
    object Loading : ApiStatus()
    object Empty : ApiStatus()
    class Success(val response: Any) : ApiStatus()
    class Failure(val error: Throwable) : ApiStatus()
}