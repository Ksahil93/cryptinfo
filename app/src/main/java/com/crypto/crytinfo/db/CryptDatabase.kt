package com.crypto.crytinfo.db


import android.content.Context
import android.os.AsyncTask
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.crypto.crytinfo.model.NewsModel
import java.io.IOException


@Database(
    entities = [NewsModel::class],
    version = 1, exportSchema = true
)
@TypeConverters(ProductTypeConverters::class)
abstract class CryptDatabase : RoomDatabase() {

    abstract fun cryptDao(): CryptDao

    companion object {
        private var instance: CryptDatabase? = null


        fun getInstance(context: Context): CryptDatabase? {
            if (instance == null) {
                synchronized(CryptDatabase::class) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        CryptDatabase::class.java, "crypt_database"
                    )
                        /*  .fallbackToDestructiveMigrationOnDowngrade()*/
                        .fallbackToDestructiveMigration()
                        .addCallback(roomCallback)
                        .build()
                }
            }
            return instance
        }

        fun destroyInstance() {
            instance = null
        }

        private val roomCallback = object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)

                try {
                    PopulateDbAsyncTask(instance)
                        .execute()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }


    }

    class PopulateDbAsyncTask(db: CryptDatabase?) : AsyncTask<Unit, Unit, Unit>() {
        private val cryptDao = db?.cryptDao()
        override fun doInBackground(vararg p0: Unit?) {
        }
    }

}