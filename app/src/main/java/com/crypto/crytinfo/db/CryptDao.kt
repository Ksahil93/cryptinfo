package com.crypto.crytinfo.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.crypto.crytinfo.model.NewsModel

@Dao
interface CryptDao {

    @Query("SELECT * FROM news_data")
    fun getAllNewsList(): NewsModel


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertNews(newsModel: NewsModel)


}