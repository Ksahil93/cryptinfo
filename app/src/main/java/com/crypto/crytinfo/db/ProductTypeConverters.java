package com.crypto.crytinfo.db;

import androidx.room.TypeConverter;

import com.crypto.crytinfo.model.NewsDataModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ProductTypeConverters {


    @TypeConverter
    public static List<NewsDataModel> stringToNews(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<NewsDataModel>>() {
        }.getType();
        List<NewsDataModel> measurements = gson.fromJson(json, type);
        return measurements;
    }

    @TypeConverter
    public static String newsToString(List<NewsDataModel> list) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<NewsDataModel>>() {
        }.getType();
        String json = gson.toJson(list, type);
        return json;
    }

    @TypeConverter
    public static ArrayList<String> stringToArrya(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        ArrayList<String> measurements = gson.fromJson(json, type);
        return measurements;
    }

    @TypeConverter
    public static String arrayToString(ArrayList<String> list) {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        String json = gson.toJson(list, type);
        return json;
    }

}