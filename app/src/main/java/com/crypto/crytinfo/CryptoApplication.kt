package com.crypto.crytinfo

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.crypto.crytinfo.utils.TypefaceUtil

class CryptoApplication : MultiDexApplication() {

    init {
        instance = this
    }

    companion object {
        private var instance: CryptoApplication? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        TypefaceUtil.overrideFont(applicationContext, "SERIF", "fonts/brandon_reg.ttf")

    }

}