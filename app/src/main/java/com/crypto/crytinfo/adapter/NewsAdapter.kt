package com.crypto.crytinfo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.crypto.crytinfo.databinding.ItemNewsBinding
import com.crypto.crytinfo.model.NewsDataModel

class NewsAdapter(private val dataList: ArrayList<NewsDataModel>) :
    RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    class NewsViewHolder(private val binding: ItemNewsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: NewsDataModel) {
            binding.apply {
                tvTitle.text = data.title
                tvSource.text = data.source
                Glide.with(storyImg.context)
                    .load(data.imgURL)
                    .into(storyImg)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        return NewsViewHolder(
            ItemNewsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }


    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val data = dataList[position]
        holder.bind(data)
    }


}