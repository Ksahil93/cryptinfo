package com.crypto.crytinfo.adapter

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.crypto.crytinfo.R
import com.crypto.crytinfo.databinding.ItemHomeCardBinding
import com.crypto.crytinfo.databinding.ItemHomeExchangeCardBinding
import com.crypto.crytinfo.model.CoinDataModel
import com.crypto.crytinfo.model.ExchangeDataModel
import com.crypto.crytinfo.model.TrendingCoinModel

class MarketHorizontalListAdapter(private val dataList: List<*>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private val GLOBAL = 0
    private val COINS = 1
    private val EXCHANGE = 2

    class MarketViewHolder(private val binding: ItemHomeCardBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Any) {
            binding.apply {
                when (data) {
                    is CoinDataModel -> {
                        Glide.with(logo.context)
                            .load(data.image)
                            .into(logo)
                        tvName.text = data.name
                        tvPrice.text = "$ " + data.current_price
                        tvPerct.text = data.price_change_percentage_24h.toString()
                        if (data.price_change_percentage_24h!! > 0) {
                            tvPerct.setTextColor(
                                tvPerct.context.resources.getColor(
                                    R.color.colorGreen,
                                    null
                                )
                            )
                            graph.gradientFillColors =
                                intArrayOf(
                                    Color.parseColor("#81177502"),
                                    Color.TRANSPARENT
                                )
                        } else {
                            tvPerct.setTextColor(Color.RED)
                            graph.lineColor = Color.RED
                            graph.gradientFillColors =
                                intArrayOf(
                                    Color.parseColor("#81D0000E"),
                                    Color.TRANSPARENT
                                )
                        }
                        graph.animation.duration = 1000L
                        if (data.sparkline_in_7d?.price != null && data.sparkline_in_7d?.price?.size!! > 0) {
                            Log.e("!!!index", data.sparkline_in_7d?.price?.size.toString())
                            val graphlist = ArrayList<Pair<String, Float>>()
                            for (i in data.sparkline_in_7d?.price?.indices!!) {
                                Log.e("!!!index", i.toString())
                                graphlist.add(Pair(i.toString(), data.sparkline_in_7d?.price!![i]))
                            }
                            graph.animate(graphlist)
                        }

                    }
                    is TrendingCoinModel -> {
                        Glide.with(logo.context)
                            .load(data.item.small)
                            .into(logo)
                        tvName.text = data.item.name
                        tvPrice.text = data.item.symbol
                        tvPerct.text = "Rank " + data.item.market_cap_rank

                    }
                }
            }
        }
    }

    class ExchangeViewHolder(private val binding: ItemHomeExchangeCardBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Any) {
            binding.apply {
                when (data) {
                    is ExchangeDataModel -> {
                        Glide.with(logo.context)
                            .load(data.image)
                            .into(logo)
                        tvName.text = data.name
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            COINS -> {
                return MarketViewHolder(
                    ItemHomeCardBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
            EXCHANGE -> {
                return ExchangeViewHolder(
                    ItemHomeExchangeCardBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
            else -> {
                return MarketViewHolder(
                    ItemHomeCardBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
        }
    }


    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun getItemViewType(position: Int): Int {
        val data = dataList[position]
        when (data) {
            is CoinDataModel -> return COINS
            is TrendingCoinModel -> return COINS
            is ExchangeDataModel -> return EXCHANGE
            else -> return COINS

        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = dataList[position]
        when (holder.itemViewType) {
            COINS -> {
                val viewHolder = holder as MarketViewHolder
                viewHolder.bind(data!!)
            }
            EXCHANGE -> {
                val viewHolder = holder as ExchangeViewHolder
                viewHolder.bind(data!!)
            }
        }
    }
}