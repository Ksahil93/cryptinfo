package com.crypto.crytinfo.adapter

import android.view.LayoutInflater
import android.view.View.GONE
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.covid.covaxx.utils.Constant
import com.crypto.crytinfo.databinding.ItemHomeBinding
import com.crypto.crytinfo.model.HomeResponseModel

class MarketAdapter(private val dataList: ArrayList<HomeResponseModel>) :
    RecyclerView.Adapter<MarketAdapter.MarketViewHolder>() {


    class MarketViewHolder(private val binding: ItemHomeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: HomeResponseModel) {
            val adapterHorizontal = MarketHorizontalListAdapter(data.data)
            binding.apply {
                tvSection.text = data.title
                if (data.title.equals(Constant.TRENDING)) {
                    tvSee.visibility = GONE
                }
                rvCoins.apply {
                    setHasFixedSize(true)
                    layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
                    adapter = adapterHorizontal
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MarketViewHolder {
        return MarketViewHolder(
            ItemHomeBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }


    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: MarketViewHolder, position: Int) {
        val data = dataList[position]
        holder.bind(data)
    }


}