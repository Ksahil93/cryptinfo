package com.crypto.crytinfo.ui.fragment.news

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.crypto.crytinfo.network.ApiStatus
import com.crypto.crytinfo.network.CryptoRepository
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class NewsViewModel : ViewModel() {

    private val _apiStateFlow: MutableStateFlow<ApiStatus> = MutableStateFlow(ApiStatus.Empty)
    val apiStateFlow: StateFlow<ApiStatus> = _apiStateFlow
    private val mainRepository = CryptoRepository()

    fun getNews() = viewModelScope.launch {
        mainRepository.getNews()
            .onStart {
                _apiStateFlow.value = ApiStatus.Loading
            }
            .catch { e ->
                _apiStateFlow.value = ApiStatus.Failure(e)
            }
            .collect { result ->
                _apiStateFlow.value = ApiStatus.Success(result as Any)
            }
    }
}