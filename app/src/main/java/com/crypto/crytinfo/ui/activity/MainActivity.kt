package com.crypto.crytinfo.ui.activity

import android.os.Bundle
import android.util.Log
import android.view.View.INVISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.covid.covaxx.utils.Constant
import com.crypto.crytinfo.R
import com.crypto.crytinfo.databinding.ActivityMainBinding
import com.crypto.crytinfo.ui.fragment.explore.ExploreFragment
import com.crypto.crytinfo.ui.fragment.market.MarketFragment
import com.crypto.crytinfo.ui.fragment.news.NewsFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private var marketFragment: MarketFragment? = null
    private var newsFragment: NewsFragment? = null
    private var moreFragment: ExploreFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        marketFragment = MarketFragment()
        buildFragmentsList(Constant.MARKET, marketFragment)

        binding.toolbar.btnBack.visibility = INVISIBLE

        binding.navView.setOnNavigationItemSelectedListener { item ->
            Log.e("!!!item", "CLICK")
            when (item.itemId) {
                R.id.navigation_market -> {
                    if (marketFragment == null) {
                        marketFragment = MarketFragment()
                        buildFragmentsList(Constant.MARKET, marketFragment)
                    } else {
                        switchFragment(Constant.MARKET)
                    }
                }

                R.id.navigation_news -> {
                    Log.e("!!!item", "CLICKNEWS")
                    if (newsFragment == null) {
                        newsFragment = NewsFragment()
                        buildFragmentsList(Constant.NEWS, newsFragment)
                    } else {
                        switchFragment(Constant.NEWS)
                    }
                }

                R.id.navigation_more -> {
                    if (moreFragment == null) {
                        moreFragment = ExploreFragment()
                        buildFragmentsList(Constant.MORE, moreFragment)
                    } else {
                        switchFragment(Constant.MORE)
                    }
                }

            }
            true
        }
    }

    private fun buildFragmentsList(tag: String, fragment: Fragment?) {
        if (fragment != null && !fragment.isAdded) {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.container, fragment, tag)
            transaction.addToBackStack(tag)
            transaction.commitAllowingStateLoss()
        }
    }

    private fun switchFragment(tag: String) {
        if (supportFragmentManager.findFragmentByTag(tag) != null) {
            val frag = supportFragmentManager.findFragmentByTag(tag) as Fragment
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, frag, tag)
                .addToBackStack(tag)
                .commitAllowingStateLoss()
        }
    }
}