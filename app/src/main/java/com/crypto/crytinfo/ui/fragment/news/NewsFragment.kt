package com.crypto.crytinfo.ui.fragment.news

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.crypto.crytinfo.adapter.NewsAdapter
import com.crypto.crytinfo.databinding.FragmentNewsBinding
import com.crypto.crytinfo.model.NewsDataModel
import com.crypto.crytinfo.model.NewsModel
import com.crypto.crytinfo.network.ApiStatus
import kotlinx.coroutines.flow.collect

class NewsFragment : Fragment() {

    private lateinit var newsViewModel: NewsViewModel
    private lateinit var binding: FragmentNewsBinding
    private lateinit var newsList: ArrayList<NewsDataModel>
    private var newsAdapter: NewsAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        newsViewModel =
            ViewModelProvider(this).get(NewsViewModel::class.java)

        binding = FragmentNewsBinding.inflate(inflater, container, false)


        newsList = ArrayList()
        initRecyclerview()
        newsViewModel.getNews()
        handleApiResponse()


        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.e("!!!Destroy","View destroy")
    }


    private fun initRecyclerview() {
        newsAdapter = NewsAdapter(newsList)
        binding.apply {
            newsRecyclerview.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext())
                adapter = newsAdapter
            }
        }
    }

    private fun handleApiResponse() {
        lifecycleScope.launchWhenStarted {
            newsViewModel.apiStateFlow.collect {
                when (it) {
                    is ApiStatus.Loading -> {
                        binding.apply {
                            Log.d("news", "handleResponse: LOADING")
                        }
                    }
                    is ApiStatus.Success -> {
                        binding.apply {
                            if (it.response is NewsModel) {
                                newsList.addAll(it.response.news)
                                newsAdapter?.notifyDataSetChanged()
                            }
                        }
                        Log.e("!!news", "handleResponse: ${it.response}")
                    }
                    is ApiStatus.Failure -> {
                        binding.apply {
                            Log.d("news", "handleResponse: EXCEPTION" + it.error.localizedMessage)
                        }

                    }
                    is ApiStatus.Empty -> {

                    }
                }
            }
        }
    }


}