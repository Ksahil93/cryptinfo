package com.crypto.crytinfo.ui.fragment.market

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.crypto.crytinfo.R
import com.crypto.crytinfo.adapter.MarketAdapter
import com.crypto.crytinfo.databinding.FragmentMarketBinding
import com.crypto.crytinfo.model.GlobalModel
import com.crypto.crytinfo.model.HomeResponseModel
import com.crypto.crytinfo.network.ApiStatus
import com.crypto.crytinfo.utils.ApiUrl
import kotlinx.coroutines.flow.collect

class MarketFragment : Fragment() {

    private lateinit var marketViewModel: MarketViewModel
    private lateinit var binding: FragmentMarketBinding
    private lateinit var marketList: ArrayList<HomeResponseModel>

    private var marketAdapter: MarketAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        marketViewModel =
            ViewModelProvider(this).get(MarketViewModel::class.java)

        binding = FragmentMarketBinding.inflate(inflater, container, false)

            marketList = ArrayList()
            initRecyclerview()
            makeAPICall()
            handleApiResponse()


        return binding.root
    }

    private fun makeAPICall() {
        var params = ApiUrl.PAGE_PARAMS.replace("{count}", "7")
        params = params.replace("{pageno}", "1")
        marketViewModel.getGlobal()
        marketViewModel.getCryptocurrency(params)
        marketViewModel.getTending()
        marketViewModel.getExchange(params)
    }

    private fun initRecyclerview() {
        marketAdapter = MarketAdapter(marketList)
        binding.apply {
            rvHome.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext())
                adapter = marketAdapter
            }
        }
    }

    private fun handleApiResponse() {
        lifecycleScope.launchWhenStarted {
            marketViewModel.apiStateFlow.collect {
                when (it) {
                    is ApiStatus.Loading -> {
                        binding.apply {
                            Log.d("main", "handleResponse: LOADING")
                        }
                    }
                    is ApiStatus.Success -> {
                        binding.apply {
                            when (it.response) {
                                is HomeResponseModel -> {
                                    marketList.add(it.response)
                                    marketAdapter?.notifyDataSetChanged()
                                }
                                is GlobalModel -> {
                                    global.tvActive.text =
                                        it.response.data.active_cryptocurrencies.toString()
                                    global.tvEnded.text = it.response.data.ended_icos.toString()
                                    global.tvOngoing.text = it.response.data.ongoing_icos.toString()
                                    global.tvUpcoming.text =
                                        it.response.data.upcoming_icos.toString()
                                    global.tvMarket.text = it.response.data.markets.toString()
                                    global.tvChange.text =
                                        it.response.data.market_cap_change_percentage_24h_usd.toString()
                                    if (it.response.data.market_cap_change_percentage_24h_usd > 0) {
                                        context?.resources?.let { it1 ->
                                            global.tvChange.setTextColor(
                                                it1.getColor(
                                                    R.color.colorGreen,
                                                    null
                                                )
                                            )
                                        }
                                    } else {
                                        global.tvChange.setTextColor(Color.RED)
                                    }
                                    global.tvTotal.text =
                                        "BTC " + it.response.data.total_market_cap.btc
                                }
                            }
                        }
                        Log.e("!!main", "handleResponse: ${it.response}")
                    }
                    is ApiStatus.Failure -> {
                        binding.apply {
                            Log.d("main", "handleResponse: EXCEPTION")
                        }

                    }
                    is ApiStatus.Empty -> {

                    }
                }
            }
        }
    }


}