package com.crypto.crytinfo.ui.fragment.market

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.covid.covaxx.utils.Constant
import com.crypto.crytinfo.model.HomeResponseModel
import com.crypto.crytinfo.network.ApiStatus
import com.crypto.crytinfo.network.CryptoRepository
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class MarketViewModel : ViewModel() {

    private val _apiStateFlow: MutableStateFlow<ApiStatus> = MutableStateFlow(ApiStatus.Empty)
    val apiStateFlow: StateFlow<ApiStatus> = _apiStateFlow
    private val mainRepository = CryptoRepository()


    fun getCryptocurrency(params: String) = viewModelScope.launch {
        mainRepository.getCryptocurrency(params)
            .onStart {
                _apiStateFlow.value = ApiStatus.Loading
            }
            .catch { e ->
                _apiStateFlow.value = ApiStatus.Failure(e)
            }
            .collect { result ->
                Log.d("main", "handleResponse:CRYPTO")
                val data = HomeResponseModel(0, Constant.CRYPTO, result)
                _apiStateFlow.value = ApiStatus.Success(data as Any)
            }
    }

    fun getExchange(params: String) = viewModelScope.launch {
        mainRepository.getExchange(params)
            .onStart {
                _apiStateFlow.value = ApiStatus.Loading
            }
            .catch { e ->
                _apiStateFlow.value = ApiStatus.Failure(e)
            }
            .collect { result ->
                val data = HomeResponseModel(1, Constant.EXCHANGE, result)
                _apiStateFlow.value = ApiStatus.Success(data as Any)
            }
    }

    fun getTending() = viewModelScope.launch {
        mainRepository.getTrending()
            .onStart {
                _apiStateFlow.value = ApiStatus.Loading
            }
            .catch { e ->
                _apiStateFlow.value = ApiStatus.Failure(e)
            }
            .collect { result ->
                val data = HomeResponseModel(1, Constant.TRENDING, result.coins)
                _apiStateFlow.value = ApiStatus.Success(data as Any)
            }
    }

    fun getGlobal() = viewModelScope.launch {
        mainRepository.getGlobal()
            .onStart {
                _apiStateFlow.value = ApiStatus.Loading
            }
            .catch { e ->
                _apiStateFlow.value = ApiStatus.Failure(e)
            }
            .collect { result ->
                _apiStateFlow.value = ApiStatus.Success(result as Any)
            }
    }


}